defmodule ExFun2ms do
  @moduledoc """
  Documentation for ExFun2ms.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ExFun2ms.hello()
      :world

  """
  def hello do
    :world
  end
end
