defmodule ExFun2ms.CLI do
  @moduledoc """
  Documentation for ExFun2ms.CLI
  """

  @doc """
  """
  def main(argv) do
    process argv
  end

  defp process([]) do
    # display help
    IO.puts """
    $ ex_fun2ms "fun do {x, y} when (^y > 0) or (^y < 1) -> x == 2 end"
    [{{:"$1", :"$2"}, [{:orelse, {:>, y, 0}, {:<, y, 1}}], [{:==, :"$1", 2}]}]
    """
    System.halt(0)
  end

  defp process([str]) do
    import Ex2ms, warn: false
    {ret, _bindings} =
      Regex.replace(~r/\^(\w+)/, str, ":trans_\\1")
      |> Code.eval_string([], __ENV__)
    Regex.replace(~r/:trans_(\w+)/, inspect(ret), "\\1")
    |> IO.puts
    System.halt(0)
  end

end
