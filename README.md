# ExFun2ms

convert fun to `match specification`

## Installation


```sh
    $ mix escript.install git https://gitlab.com/schemer.bai/ex_fun2ms_cli.git

    $ ex_fun2ms "fun do {x, y} when (^y > 0) or (^y < 1) -> x == 2 end"
    [{{:"$1", :"$2"}, [{:orelse, {:>, y, 0}, {:<, y, 1}}], [{:==, :"$1", 2}]}]

```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/ex_fun2ms](https://hexdocs.pm/ex_fun2ms).

